FROM ubuntu:focal
RUN ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
RUN apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y git make cmake libboost-all-dev python3 python3-pip tcl swig qt5-default python3-pyqt5 libqt5svg5-dev libxml2 curl unzip zutils
RUN mkdir -p ${HOME}/coriolis-2.x/Linux.x86_64/Release.Shared && \
    curl --output coriolis_artifacts.zip --location "https://gitlab.com/api/v4/projects/ChipFlow%2Fcoriolis-ci-build/jobs/2368786990/artifacts" && \
    unzip coriolis_artifacts.zip && \
    tar -C ${HOME}/coriolis-2.x/Linux.x86_64/Release.Shared -zxf coriolis.tgz && \
    rm coriolis_artifacts.zip coriolis.tgz && \
    ls ${HOME}/coriolis-2.x
RUN pip3 install yowasp-yosys flask
RUN git clone https://github.com/ChipFlow/mpw4 ${HOME}/mpw4 # until we have a better way of getting the PDK
RUN mkdir -p /app
COPY ./app/*.py ./app/*.sh /app/
EXPOSE 5000
ENTRYPOINT ["bash", "/app/run.sh"]
