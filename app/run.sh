#!/usr/bin/env bash
set -ex
export CORIOLIS_TOP=$HOME/coriolis-2.x/Linux.x86_64/Release.Shared/install
export LD_LIBRARY_PATH=$CORIOLIS_TOP/lib64
export PYTHONPATH=$PYTHONPATH:$CORIOLIS_TOP/lib64/python3/dist-packages:$CORIOLIS_TOP/lib64/python3/dist-packages/crlcore:$CORIOLIS_TOP/lib64/python3/dist-packages/cumulus/
export PATH=$HOME/.local/bin:$PATH
cd /app && FLASK_APP=gds_service python3 -m flask run --host=0.0.0.0

