import argparse, sys, os, subprocess
from pathlib import Path

from tempfile import mkdtemp

def sky130_rtlil_to_gds(rtlil, core_size=(240*10.0, 240*10.0)):
    pdk_dir = os.environ["PDK_DIR"] if "PDK_DIR" in os.environ else os.environ["HOME"] + "/mpw4/thirdparty/open_pdk/C4M.Sky130"
    # Create build files
    temp_dir = f"{os.getcwd()}/tmp"
    Path(temp_dir).mkdir(parents=True, exist_ok=True)
    build_dir = mkdtemp(prefix="sky130", dir=temp_dir)
    top_name = f"soc_top"
    top_rtlil = f"{build_dir}/top.il"
    with open(top_rtlil, "w") as f:
        f.write(rtlil.decode('utf-8'))
    abc_constr = f"{build_dir}/abc_sky130.constr"
    with open(abc_constr, "w") as f:
        print("set_driving_cell inv_x4", file=f)
        print("set_load 1.0", file=f)
    def run_synth():
        # Synthesise & map pre-synth RTLIL to BLIF netlist
        liberty = f"{pdk_dir}/libs.ref/StdCellLib/liberty/StdCellLib_slow.lib"
        top_ys = f"{build_dir}/sky130.ys"
        target_delay = 10000
        max_fanout = 32
        with open(top_ys, "w") as f:
            print(f"read_liberty -lib {liberty}", file=f)
            print(f"read_ilang {top_rtlil}", file=f)
            print(f"synth -flatten -auto-top", file=f)
            print(f"rename -top {top_name}", file=f)
            print(f"dfflibmap -liberty {liberty}", file=f)
            print(f"opt", file=f)
            print(f"abc -script +strash;ifraig;scorr;dc2;dretime;strash;&get,-n;&dch,-f;&nf,-D,{target_delay};&put;buffer,-G,1000,-N,{max_fanout};upsize,-D,{target_delay};dnsize,-D,{target_delay};stime,-p -constr {abc_constr} -liberty {liberty}", file=f)
            print(f"setundef -zero", file=f)
            print(f"clean -purge", file=f)
            print(f"setundef -zero", file=f)
            print(f"write_blif {build_dir}/{top_name}.blif", file=f)
            print(f"write_json {build_dir}/{top_name}.json", file=f)
            print(f"splitnets -ports", file=f) # required for tas
            print(f"write_verilog -noattr {build_dir}/{top_name}_syn.v", file=f)
            print(f"stat", file=f)
        subprocess.run(["yowasp-yosys", "-ql",f"{build_dir}/synth.log", top_ys], check=True)
    def run_pnr():
        old_cwd = os.getcwd()
        os.chdir(build_dir)
        import CRL, Hurricane as Hur, Katana, Etesian, Anabatic, Cfg
        from Hurricane import DataBase, Transformation, Box, Instance
        from helpers import u, l, setNdaTopDir
        from helpers.overlay import CfgCache, UpdateSession

        sys.path.append(f"{pdk_dir}/libs.tech/coriolis/techno/etc/coriolis2")
        from node130 import sky130 as tech
        tech.setup()
        tech.StdCellLib_setup()

        from plugins.alpha.block.block         import Block
        from plugins.alpha.block.configuration import IoPin, GaugeConf
        from plugins.alpha.block.spares        import Spares
        from plugins.alpha.chip.configuration  import ChipConf
        from plugins.alpha.chip.chip           import Chip
        from plugins.alpha.core2chip.sky130    import CoreToChip

        cell_name = top_name
        cell = CRL.Blif.load(cell_name)

        # TODO: re-add the inv script
        # from .sky130_scripts.split_inv_clocks import split_inv_clocks
        # split_inv_clocks(cell)

        af = CRL.AllianceFramework.get()
        env = af.getEnvironment()
        env.setCLOCK('io_in_from_pad(0)')

        lg5 = af.getRoutingGauge('StdCellLib').getLayerGauge( 5 )
        lg5.setType( CRL.RoutingLayerGauge.PowerSupply )

        conf = ChipConf( cell, ioPins=[], ioPads=[] ) 
        conf.cfg.etesian.bloat               = 'Flexlib'
        conf.cfg.etesian.uniformDensity      = True
        conf.cfg.etesian.aspectRatio         = 1.0
       # etesian.spaceMargin is ignored if the coreSize is directly set.
        conf.cfg.etesian.spaceMargin         = 0.10
        conf.cfg.etesian.antennaGateMaxWL = u(400.0)
        conf.cfg.etesian.antennaDiodeMaxWL = u(800.0)
        conf.cfg.etesian.feedNames = 'tie_diff,tie_diff_w2'
        conf.cfg.anabatic.searchHalo         = 2
        conf.cfg.anabatic.globalIterations   = 20
        conf.cfg.anabatic.topRoutingLayer    = 'm4'
        conf.cfg.katana.hTracksReservedLocal = 25
        conf.cfg.katana.vTracksReservedLocal = 20
        conf.cfg.katana.hTracksReservedMin   = 12
        conf.cfg.katana.vTracksReservedMin   = 10
        conf.cfg.katana.trackFill            = 0
        conf.cfg.katana.runRealignStage      = True
        conf.cfg.katana.dumpMeasures         = True
        conf.cfg.katana.longWireUpReserve1   = 3.0
        conf.cfg.block.spareSide             = u(7*10)
        conf.cfg.chip.minPadSpacing          = u(1.46)
        conf.cfg.chip.supplyRailWidth        = u(20.0)
        conf.cfg.chip.supplyRailPitch        = u(40.0)
        conf.cfg.harness.path                = f"{pdk_dir}/../../../resources/user_project_wrapper.def" # TODO: configure
        conf.useSpares           = True
        # conf.useClockTree        = True
        # conf.useHFNS             = True
        conf.bColumns            = 2
        conf.bRows               = 2
        conf.chipName            = 'chip'
        conf.coreSize            = ( u(core_size[0]), u(core_size[1]) )
        conf.useHTree( 'io_in_from_pad(0)', Spares.HEAVY_LEAF_LOAD ) # TODO: configure

        coreToChip = CoreToChip( conf )
        coreToChip.buildChip()
        chipBuilder = Chip( conf )
        chipBuilder.doChipFloorplan()
        chipBuilder.doPnR()
        chipBuilder.save()

        # Write LEF and DEF for PEX etc
        Path("export").mkdir(parents=True, exist_ok=True)
        os.chdir("export")
        db = DataBase.getDB()
        rootlib = db.getRootLibrary()
        lib = rootlib.getLibrary("StdCellLib")
        CRL.LefExport.drive(lib, 1)
        CRL.DefExport.drive(conf.corona, 0)
        for cell in lib.getCells():
            if cell.getName() in (conf.corona.getName(), conf.core.getName(), conf.chip.getName()):
                continue
            CRL.DefExport.drive(cell, 0)
        os.chdir(old_cwd)
    run_synth()
    run_pnr()
    return build_dir