
from builder import sky130_rtlil_to_gds

from flask import Flask, render_template, request, send_file

app = Flask(__name__)

@app.route('/build', methods=['POST'])
def parse_request():
    data = request.stream.read()
    # This is horrid. It just does the build synchronously in the web server thread and blocks synchronously until done
    result_dir = sky130_rtlil_to_gds(data)
    # Return the build GDS
    return send_file(f"{result_dir}/user_project_wrapper.gds", mimetype='application/octet-stream')
